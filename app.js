const app = document.querySelector('.app')
let tasksArray = []

class Button {
  constructor(text, className, type = 'button') {
    this.text = text
    this.className = className
    this.type = type
  }

  static onClick(e) {
    const showAllBtn = document.querySelector('.all')
    const showActiveBtn = document.querySelector('.active')
    const showFinishedBtn = document.querySelector('.finished')

    const filterButtonsContainer = document.querySelector(
      '.filter-buttons-container'
    )
    const deleteAllButton = document.querySelector('.delete-all-btn')
    const emptyTodoAlert = document.querySelector('.empty-list-alert ')

    if (e.target.classList.contains('delete-btn')) {
      const taskDiv = e.target.closest('.task')

      tasksArray = tasksArray.filter((item) => item.id !== taskDiv.dataset.id)

      taskDiv.remove()

      if (!tasksArray.length) {
        filterButtonsContainer.style.display = 'none'
        deleteAllButton.style.display = 'none'
        emptyTodoAlert.style.display = 'block'
      }
    }

    if (e.target.classList.contains('cancel-task-btn')) {
      const input = document.querySelector('.task-input')
      input.value = ''
    }

    if (e.target.classList.contains('delete-all-btn')) {
      tasksArray = []

      const tasksNodes = document.querySelectorAll('.task')
      const taskArray = [...tasksNodes]

      taskArray.forEach((task) => {
        task.remove()
      })

      deleteAllButton.style.display = 'none'
      emptyTodoAlert.style.display = 'block'
      filterButtonsContainer.style.display = 'none'
    }

    // Status change
    if (e.target.classList.contains('status-change-btn')) {
      const taskDiv = e.target.closest('.task')
      const messageParagraph = taskDiv.querySelector('.task-message')
      const taskDivNodes = document.querySelectorAll('.task')
      const taskDivArray = [...taskDivNodes]

      if (messageParagraph.style.textDecoration === 'line-through') {
        messageParagraph.style.textDecoration = 'none'
        taskDiv.dataset.status = 'active'

        tasksArray = tasksArray.map((el) => {
          if (el.id === `${taskDiv.dataset.id}`) {
            return {
              ...el,
              status: 'active'
            }
          }
          return el
        })

        e.target.classList.remove('highlight-red')
        e.target.classList.add('main-black')

        if (
          document
            .querySelector('.finished')
            .classList.contains('highlight-red')
        ) {
          taskDivArray.forEach((div) => {
            if (div.dataset.status === 'active') {
              div.classList.add('hide-element')
            }
          })
        }

        if (
          document.querySelector('.all').classList.contains('highlight-red')
        ) {
          return
        }
      } else {
        taskDiv.dataset.status = 'finished'
        messageParagraph.style.textDecoration = 'line-through'

        tasksArray = tasksArray.map((el) => {
          if (el.id === `${taskDiv.dataset.id}`) {
            return {
              ...el,
              status: 'finished'
            }
          }
          return el
        })

        e.target.classList.remove('main-black')
        e.target.classList.add('highlight-red')

        if (
          document.querySelector('.active').classList.contains('highlight-red')
        ) {
          taskDivArray.forEach((div) => {
            if (div.dataset.status === 'finished') {
              div.classList.add('hide-element')
            }
          })
        }

        if (
          document.querySelector('.all').classList.contains('highlight-red')
        ) {
          return
        }
      }
    }

    if (e.target.classList.contains('task-message')) {
      const taskDiv = e.target.closest('.task')
      const editInput = taskDiv.querySelector('.edit-message-input')
      const messageParagraph = taskDiv.querySelector('.task-message')

      editInput.value = messageParagraph.textContent

      messageParagraph.style.display = 'none'
      editInput.style.display = 'block'
      editInput.focus()

      const allMessageParagraphs = document.querySelectorAll('.task-message')
      const paragraphArray = [...allMessageParagraphs]

      paragraphArray.forEach((par) => {
        if (par !== messageParagraph) {
          par.classList.remove('hide-element')

          const task = par.closest('.task')

          task.querySelector('.edit-message-input').style.display = 'none'
          task.querySelector('.task-message').style.display = 'block'
        }
      })
    }

    if (e.target.classList.contains('filter-btn')) {
      const taskArray = document.querySelectorAll('.task')
      const taskDivArray = [...taskArray]

      if (e.target.textContent === 'All') {
        e.target.classList.add('highlight-red')
        e.target.classList.remove('main-black')
        showActiveBtn.classList.remove('highlight-red')
        showFinishedBtn.classList.remove('highlight-red')

        taskDivArray.forEach((div) => {
          div.classList.remove('hide-element')
        })
      } else if (e.target.textContent === 'Active') {
        e.target.classList.add('highlight-red')
        e.target.classList.remove('main-black')
        showFinishedBtn.classList.remove('highlight-red')
        showAllBtn.classList.remove('highlight-red')

        taskDivArray.forEach((div) => {
          if (div.dataset.status !== 'active') {
            div.classList.add('hide-element')
          } else {
            div.classList.remove('hide-element')
          }
        })
      } else if (e.target.textContent === 'Finished') {
        e.target.classList.add('highlight-red')
        e.target.classList.remove('main-black')
        showActiveBtn.classList.remove('highlight-red')
        showAllBtn.classList.remove('highlight-red')

        taskDivArray.forEach((div) => {
          if (div.dataset.status === 'active') {
            div.classList.add('hide-element')
          } else {
            div.classList.remove('hide-element')
          }
        })
      }
    }
  }

  render() {
    const newButton = document.createElement('button')
    newButton.classList.add(this.className)
    newButton.textContent = this.text
    newButton.type = this.type

    return newButton
  }
}

class Input {
  constructor(className) {
    this.className = className
  }

  static onSubmit(e) {
    e.preventDefault()
    const input = document.querySelector('.task-input')
    const taskContainer = document.querySelector('.tasks-container')
    const emptyTodoAlert = document.querySelector('.empty-list-alert')
    const deleteAllButton = document.querySelector('.delete-all-btn')
    const filterButtonsContainer = document.querySelector(
      '.filter-buttons-container'
    )

    let { value } = input
    value = value.trim()

    if (value.length < 1) {
      input.value = ''
      return
    }

    const newTaskObject = {
      message: value,
      status: 'active',
      id: Math.random().toFixed(5),
      createdAt: Date.now()
    }

    input.value = ''

    const task = taskContainer.appendChild(document.createElement('div'))
    task.classList.add('task')
    task.dataset.id = newTaskObject.id
    task.dataset.status = newTaskObject.status

    tasksArray.push(newTaskObject)
    emptyTodoAlert.style.display = 'none'
    deleteAllButton.style.display = 'block'
    filterButtonsContainer.style.display = 'block'

    const paragraph = task.appendChild(document.createElement('p'))
    paragraph.classList.add('task-message')
    paragraph.textContent = newTaskObject.message

    const editInput = task.appendChild(document.createElement('input'))
    editInput.classList.add('edit-message-input')
    editInput.style.display = 'none'

    task.appendChild(new Button('√', 'status-change-btn').render())

    task.appendChild(new Button('X', 'delete-btn').render())

    // app.addEventListener('click', (event) => {
    //   Button.onClick(event)
    // })
  }

  static onKeyUp(e) {
    const taskDiv = e.target.closest('.task')
    const editInput = taskDiv.querySelector('.edit-message-input')
    const messageParagraph = taskDiv.querySelector('.task-message')

    if (e.code === 'Escape') {
      messageParagraph.style.display = 'block'
      editInput.style.borderColor = 'unset'
      editInput.style.display = 'none'
      return
    }

    if (e.code !== 'Enter') return

    if (!editInput.value.trim().length) {
      editInput.style.borderColor = 'red'
      editInput.value = ''
      return
    }

    messageParagraph.textContent = editInput.value
    tasksArray = tasksArray.map((el) => {
      if (el.id === `${taskDiv.dataset.id}`) {
        return {
          ...el,
          message: editInput.value
        }
      }
      return el
    })

    editInput.style.borderColor = 'unset'
    editInput.value = ''

    messageParagraph.style.display = 'block'
    editInput.style.display = 'none'
  }

  render() {
    const input = document.createElement('input')
    input.classList.add(this.className)
    input.placeholder = 'New ToDo'

    return input
  }
}

class AppConstructor {
  constructor(parent, element, className, text) {
    this.parent = parent
    this.element = element
    this.className = className
    this.text = text

    this.render()
  }

  render() {
    const newElement = document.createElement(this.element)
    newElement.classList.add(this.className)
    newElement.textContent = this.text
    newElement.placeholder = this.placeholder

    this.parent.appendChild(newElement)

    const form = document.createElement('form')
    form.classList.add(this.className)

    const label = form.appendChild(document.createElement('label'))
    label.classList.add('task-label')
    label.textContent = 'Todos'

    const inputWrapper = form.appendChild(document.createElement('div'))
    inputWrapper.classList.add('input-wrapper')

    const input = new Input('task-input').render()

    inputWrapper.appendChild(input)

    inputWrapper.appendChild(
      new Button('Add', 'create-task-btn', 'submit').render()
    )

    inputWrapper.appendChild(new Button('Cancel', 'cancel-task-btn').render())

    newElement.appendChild(form)

    const tasksContainer = document.createElement('div')
    app.appendChild(tasksContainer)
    tasksContainer.classList.add('tasks-container')

    const emptyTodoAlert = tasksContainer.appendChild(
      document.createElement('div')
    )
    emptyTodoAlert.classList.add('empty-list-alert')
    emptyTodoAlert.textContent = 'No Todos'

    tasksContainer.appendChild(
      new Button('Clear All', 'delete-all-btn').render()
    )

    const filterButtonsContainer = tasksContainer.appendChild(
      document.createElement('div')
    )
    filterButtonsContainer.classList.add('filter-buttons-container')
    filterButtonsContainer.style.display = 'none'

    const showAllBtn = filterButtonsContainer.appendChild(
      new Button('All', 'filter-btn').render()
    )
    showAllBtn.classList.add('all')
    showAllBtn.classList.add('highlight-red')

    const showActiveBtn = filterButtonsContainer.appendChild(
      new Button('Active', 'filter-btn').render()
    )
    showActiveBtn.classList.add('active')

    const showFinishedBtn = filterButtonsContainer.appendChild(
      new Button('Finished', 'filter-btn').render()
    )
    showFinishedBtn.classList.add('finished')

    this.node = newElement

    app.addEventListener('submit', (e) => {
      Input.onSubmit(e)
    })

    app.addEventListener('click', (event) => {
      Button.onClick(event)
    })

    app.addEventListener('keyup', (event) => {
      if (!event.target.classList.contains('edit-message-input')) return
      Input.onKeyUp(event)
    })
  }
}

const appTemplate = new AppConstructor(app, 'div', 'task-panel')
